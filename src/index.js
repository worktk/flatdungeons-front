import React from "react"
import ReactDom from "react-dom"
import { Helmet } from "react-helmet"
import Favicon from "react-favicon"

import Menu from "./statics/menu"
import Login from "./auth/login"
import Cookie from "js-cookie"
import axios from "axios"
import BodyPage from "./BodyPage"



class MainPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = { user: [], nav: "home", title: "Home - Flat Dungeons" };
        if (Cookie.get("token") !== undefined) {
            this.updateUserData();
        }
        

    }

    updateUserData() {
        const response = axios.post(
            'http://localhost:5000/api/auth/token',
            { token: Cookie.get("token") },
            { headers: { 'Content-Type': 'application/json' } }
        )

        response.then(response => {
            this.setState({ user: response.data });
            console.log(this.state.user)
        }).catch(error => console.log(error))

    }


    updateUserOnLogin = (childData) => {
        this.setState({ user: childData })
    };

    logoutUser() {
        this.setState({ user: [] })
    }

    updateNav = (navi) => {
        this.setState({ nav: navi })
    }


    updateTitle = (newtitle) => {
        this.setState({ title: newtitle + " - Flat Dungeons" })
    }



    render() {

        if (Cookie.get("token") !== undefined && this.state.user.length !== 0) {
            return (
                <div className="divbody">
                    <Favicon path="favicon.ico" />
                    <Helmet>
                        <title>{this.state.title}</title>
                    </Helmet>
                    <Menu user={this.state.user} logoutUser={this.logoutUser.bind(this)} updateNav={this.updateNav.bind(this)} />
                    <div id="right-panel" className="rightPanel">
                        <BodyPage nav={this.state.nav} user={this.state.user} />
                    </div>
                </div>
            )
        } else {
            return (
                <div className="bg-dark">
                    <Helmet>
                        <title>{this.state.title}</title>
                    </Helmet>
                    <Login parentCallback={this.updateUserOnLogin} updateTitle={this.updateTitle} />
                </div>
            )
        }

    }

}


ReactDom.render(
    <MainPage />,
    document.getElementById("root")
)
