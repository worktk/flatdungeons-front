import React, { Component } from "react"
import axios from "axios"
import Cookie from "js-cookie"
class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = { login: '', pass: '', email: '', data: [], page: 'login' };
        this.handleLogin = this.handleLogin.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.props.updateTitle("Login");
        this.changePage = this.changePage.bind(this);
        this.handleRecovery = this.handleRecovery.bind(this);
        
    }

    token = Cookie.get("token") ? Cookie.get("token") : null;

    handleLogin(event) {
        this.setState({ login: event.target.value });
    }

    handlePassword(event) {
        this.setState({ pass: event.target.value });
    }

    handleEmail(event) {
        this.setState({ email: event.target.value });
    }


    handleSubmit(event) {
        const response = axios.post(
            'http://localhost:5000/api/auth/signin',
            { usernameOrEmail: this.state.login, password: this.state.pass },
            { headers: { 'Content-Type': 'application/json' } }
        )

        

        response.then(response => {
            this.setState({ data: response.data });
            Cookie.set("token", this.state.data.jwtAuthenticationResponse.accessToken, { expires: 1 });

            this.props.parentCallback(this.state.data.user)

        }).catch(error => console.log(error))


        event.preventDefault();
    }

    handleRecovery(event) {
        const response = axios.post(
            'http://localhost:5000/api/auth/resetpassword',
            {
                email: this.state.email
            },
            { headers: { 'Content-Type': 'application/json' } }
        )

        response.then(response => {
            this.setState({ data: response.data });
        }).catch(error => console.log(error))

        window.location.reload();
        event.preventDefault();
    }

    handleRegister(event) {
        const response = axios.post(
            'http://localhost:5000/api/auth/signup',
            {
                name: this.state.login,
                username: this.state.login,
                email: this.state.email,
                password: this.state.pass
            },
            { headers: { 'Content-Type': 'application/json' } }
        )

        response.then(response => {
            this.setState({ data: response.data });
        }).catch(error => console.log(error))

        window.location.reload();
        event.preventDefault();
    }

    changePage(e,newpage) {
        this.setState ({page: newpage });
    }
 

    render() {

            if (this.state.page === 'login') {
            return (
                
                <div className="sufee-login d-flex align-content-center flex-wrap">
                    
                    <div className="container">
                        <div className="login-content">
                            <div className="login-logo">
                                <img className="align-content" src="images/logo.png" alt="" />
                            </div>
                            <div className="login-form">
                            
                                
                                <form onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        <label>Login / email</label>
                                        <input name="login" id="login" type="textbox" className="form-control" onChange={this.handleLogin.bind(this)} />
                                    </div>
                                    <div className="form-group">
                                        <label>Hasło</label>
                                        <input name="password" id="password" type="password" className="form-control" onChange={this.handlePassword.bind(this)} />
                                    </div>
                                    <div className="checkbox">
                                        <label>
                                            <input type="checkbox" /> Pamiętaj mnie
                                    </label>
                                        <label className="pull-right">
                                            
                                            <a href="#" onClick={(e) => this.changePage(e,'password')}>Zapomniałeś hasła?</a>
                                            
                                        </label>

                                    </div>
                                    <input type="submit" className="btn btn-success btn-flat m-b-30 m-t-30" value="Zaloguj" />
                                    <div className="register-link m-t-15 text-center">
                                        <p>Nie masz konta? <a href="#" onClick={(e) => this.changePage(e,'register')}> Zarejestruj się tutaj</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        else if(this.state.page === 'register') {
            return (
                <div className="sufee-login d-flex align-content-center flex-wrap">
                    <div className="container">
                        <div className="login-content">
                            <div className="login-logo">
                                <img className="align-content" src="images/logo.png" alt="" />
                            </div>
                            <div className="login-form">
                                <form onSubmit={this.handleRegister.bind(this)}>
                                    <div className="form-group">
                                        <label>Login:</label>
                                        <input name="login" id="login" type="login" className="form-control" onChange={this.handleLogin.bind(this)} />
                                    </div>
                                    <div className="form-group">
                                        <label>Hasło</label>
                                        <input name="password" id="password" type="password" className="form-control" onChange={this.handlePassword.bind(this)} />
                                    </div>
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input name="email" id="email" type="email" className="form-control" onChange={this.handleEmail.bind(this)} />
                                    </div>
                                    <div className="checkbox">
                                        <label>
                                            <input type="checkbox" /> Zgadzam się oddać duszę
                                        </label>
                                    </div>
                                    <input type="submit" className="btn btn-success btn-flat m-b-30 m-t-30" value="Zarejestruj" />
                                    <div className="register-link m-t-15 text-center">
                                        <p>Masz już konto? <a href="#" onClick={(e) => this.changePage(e,'login')}> Zaloguj się</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else if (this.state.page === 'password') { 
            return (
            <div className="sufee-login d-flex align-content-center flex-wrap">
                <div className="container">
                    <div className="login-content">
                        <div className="login-logo">
                            <a href="index.html">
                                <img className="align-content" src="images/logo.png" alt="" />
                            </a>
                        </div>
                        <div className="login-form">
                            <form onSubmit={this.handleRecovery.bind(this)}>
                                <div className="form-group">
                                    <label>Email address</label>
                                    <input name="email" id="email" type="email" className="form-control" placeholder="Email" onChange={this.handleEmail.bind(this)}  />
                                </div>
                                    <input type="submit" className="btn btn-primary btn-flat m-b-15" value="Resetuj" />
                                    <div className="register-link m-t-15 text-center">
                                        <p>Masz już konto? <a href="#" onClick={(e) => this.changePage(e,'login')}> Zaloguj się</a></p>
                                    </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div> )
        }
    }
}

export default Login