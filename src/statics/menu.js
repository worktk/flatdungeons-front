import React from "react"
import Cookie from "js-cookie"

import AdminMenu from "./AdminMenu";
import ProgressBar from "react-bootstrap/ProgressBar"


class Menu extends React.Component {

    constructor(props) {
        super(props)
        this.LogOut = this.LogOut.bind(this);
    }


    LogOut() {
        Cookie.remove("token");
        this.props.logoutUser();
    }

    updateNav = (navi) => {
        this.props.updateNav(navi);
    }

    render() {
        return (
            <aside id="left-panel" className="left-panel">
                <nav className="navbar navbar-expand-sm navbar-default">

                    <div className="navbar-header">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                            <i className="fa fa-bars"></i>
                        </button>
                        <img src={this.props.user.character.characterBase.avatar} alt="Logo"/>
                        <h3 className="menu-title"><center><h2>{this.props.user.username}</h2></center></h3>
                        <div>
                            <ProgressBar className="right" variant="danger" now={this.props.user.character.hp} label={this.props.user.character.hp}/>
                        </div>
                    </div>
                    <div id="main-menu" className="main-menu collapse navbar-collapse">
                        <ul className="nav navbar-nav">
                            <li className="active">
                                <a href="#" onClick={() => { this.props.updateNav("home") }}> <i className="menu-icon fa fa-dashboard"></i>Profil </a>
                            </li>
                            <li className="active">
                                <a href="#" onClick={this.LogOut}> <i className="menu-icon fa fa-dashboard"></i>Wyloguj </a>
                            </li>
                        </ul>
                    </div>
                    <AdminMenu user={this.props.user} updateNav={this.updateNav} />
                </nav>
            </aside>
        )
    }
}




export default Menu