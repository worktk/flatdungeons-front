import React from "react"


class AdminMenu extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        if (this.props.user.roles[0].name == "ROLE_ADMIN") {
            return (
                <div>
                    <h3 className="menu-title"><center><h2>Admin Menu</h2></center></h3>
                    <div id="main-menu" className="main-menu collapse navbar-collapse">
                        <ul className="nav navbar-nav">
                            <li>
                                <a href="#" onClick={() => {
                                    this.props.updateNav("admin1");
                                }
                                }> <i className="menu-icon fa fa-dashboard"></i>Admin 1 </a>
                            </li>
                            <li>
                                <a href="#" onClick={() => { this.props.updateNav("admin2") }}> <i className="menu-icon fa fa-dashboard"></i>Admin 2 </a>
                            </li>
                        </ul>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div></div>
            )
        }
    }


}



export default AdminMenu