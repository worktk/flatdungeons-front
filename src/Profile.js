import React from "react"


class Profile extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props.user.character.characterStatistic)
    }


    render() {
        return (
            <div>
                <table>
                    <tr>
                        <td>
                            WYPOSAŻENIE
                            <table className="equipment">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/p67_unique_helm_set_01_demonhunter_male.png"/>
                                    </td>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/p66_unique_amulet_001_demonhunter_male.png"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/p67_unique_shoulder_set_01_demonhunter_male.png"/>
                                    </td>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/p67_unique_chest_set_01_demonhunter_male.png"/>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/p67_unique_gloves_set_01_demonhunter_male.png"/>
                                    </td>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/p66_unique_pants_012_demonhunter_male.png"/>
                                    </td>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/p2_unique_ring_04_demonhunter_male.png"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/unique_sword_1h_set_03_x1_demonhunter_male.png"/>
                                    </td>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/p67_unique_boots_set_01_demonhunter_male.png"/>
                                    </td>
                                    <td><img
                                        src="https://blzmedia-a.akamaihd.net/d3/icons/items/large/p65_unique_crushield_102_x1_demonhunter_male.png"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td rowSpan="2">
                            <div>
                                EKWIPUNEK
                                <table className="equipment">
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <table className="statistics">
                            <tr>
                                <td className="statistics70">SIŁA:</td>
                                <td className="statistics30">{this.props.user.character.characterStatistic.strength}</td>
                            </tr>
                            <tr>
                                <td>ZRĘCZNOŚC:</td>
                                <td>{this.props.user.character.characterStatistic.dexterity}</td>
                            </tr>
                            <tr>
                                <td>WYTRZYMAŁOŚC:</td>
                                <td>{this.props.user.character.characterStatistic.durability}</td>
                            </tr>
                            <tr>
                                <td>SZCZĘŚCIE:</td>
                                <td>{this.props.user.character.characterStatistic.luck}</td>
                            </tr>
                            <tr>
                                <td>CHARYZMA:</td>
                                <td>{this.props.user.character.characterStatistic.charisma}</td>
                            </tr>
                            <tr>
                                <td>PERCEPCJA:</td>
                                <td>{this.props.user.character.characterStatistic.perception}</td>
                            </tr>
                        </table>

                    </tr>
                </table>
            </div>
        )
    }

}

export default Profile;